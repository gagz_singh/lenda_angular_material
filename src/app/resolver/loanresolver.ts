

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router, ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { LoanApiService } from '../webservice/loanapi/loanapi.service';
import { LoancalculationService } from './../provider/calculations/loancalculation.service';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../environments/environment';
import { Loan } from '../models/appmodels/loan.model';



@Injectable()
export class LoanObjectResolver implements Resolve<any> {
  constructor(
    private loanservice:LoanApiService,public localSt:LocalStorageService,public mainloanservice:LoancalculationService,public router:Router
) {

  }

  resolve(route: ActivatedRouteSnapshot,routerstate:RouterStateSnapshot){
    debugger
    let loanid=Number(routerstate.url.substr(routerstate.url.lastIndexOf('/') + 1));
    if(this.localSt.retrieve(environment.loankey)==null || loanid!=(<Loan>this.localSt.retrieve(environment.loankey)).Id)
    {
      return this.loanservice.getloansbyid(loanid).subscribe(res=>{
        if(res.ResCode==1){
          debugger
          this.localSt.store(environment.loankey,res.Data);
          this.localSt.store(environment.loankey_copy,res.Data);
          this.mainloanservice.performcalculationonloanobject(res.Data);
        }
      })
    }
    else{
      //this.mainloanservice.performcalculationonloanobject(this.localSt.retrieve(environment.loankey));
      //return true;
    }
   
  }
}