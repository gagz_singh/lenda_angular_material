import { forEach } from "@angular/router/src/utils/collection";

export function serializeobject(obj: any, ref: any) {
    debugger
    let p: Array<any> = ref.prototype.__jsonconvert__mapping__;
    let json = {};
    Object.keys(p).forEach(iter => {
        let element = p[iter];
        if (!element.isOptional) {
            json[element.jsonPropertyName] = obj[element.classPropertyName];
        }
    });
return json;
}

export function serializearray(obj:any,ref:any){
    debugger
 let jsonarr=[];
    obj.forEach(element => {
        jsonarr.push(serializeobject(element,ref));
    });
    return jsonarr;
}
