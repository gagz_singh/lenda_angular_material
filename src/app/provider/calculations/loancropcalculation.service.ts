import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from './../../../environments/environment';
import { LoanCropsEntity } from '../../models/appmodels/loan.model';

@Injectable()
export class LoancropcalculationService {
  localLoanObj:any;
  croptypes:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService) {
    this.croptypes=this.localSt.retrieve("croptypes");
    this.localLoanObj=this.localSt.retrieve(environment.loankey);
   }
//years in descending order
  calculate6yearaverage(year1:number,year2:number,year3:number,year4:number,year5:number,year6:number):number{
    return Math.round((year1+year2+year3+year4+year5+year6)/6);
}
//years in descending order starting from 3rd in row
calculate3yearaverage(year1:number,year2:number,year3:number):number{
  return Math.round((year1+year2+year3)/3);
}

 //Rebates Specific Calculations
preparerebatesitem(loanobj:any):any{
  debugger
  loanobj.CropName=this.getcropname(loanobj.CropId);
  loanobj.BuyerName=this.getbuyername(loanobj.RebatorId);
 }
preparerebatesitemfromArray(loanobj:Array<any>):Array<any>{
  loanobj.forEach(element => {
    element=this.preparerebatesitem(element);
   });
   return loanobj;
 }
 getcropname(id:number){
  return this.croptypes.find(p=>p.id==id).value;
}

getbuyername(id:number){
  if(id!=undefined)
  return this.localLoanObj.ContractEntities.find(p=>p.Id==id).Name;
  else
  "";
}
 //////////////////////



 //Loancrop specific calculations

 prepareloancropitem(loanobj:LoanCropsEntity):any{
  loanobj.Avg3=this.calculate3yearaverage(loanobj.ProductionHistory1,loanobj.ProductionHistory2,loanobj.ProductionHistory3);
  loanobj.Avg6=this.calculate6yearaverage(loanobj.ProductionHistory1,loanobj.ProductionHistory2,loanobj.ProductionHistory3,loanobj.ProductionHistory4,loanobj.ProductionHistory5,loanobj.ProductionHistory6);
  loanobj.CropName=this.getcropname(loanobj.CropId);
 }
 prepareloancropitemfromArray(loanobj:Array<any>):Array<any>{
  loanobj.forEach(element => {
    element=this.prepareloancropitem(element);
   });
   return loanobj;
 }
 ///
}


