import { Injectable } from '@angular/core';
import { InsurancePolicies, Loan, CropMPCI } from './../../models/appmodels/loan.model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment';
import { element } from 'protractor';
import { InsuranceType } from '../../models/enums/enums';

@Injectable()
export class InsuranceCalculationService {
   localloanobject:Loan;
   croptypes:Array<any>=new Array<any>();
  constructor(public localst:LocalStorageService) {
    this.croptypes=this.localst.retrieve("croptypes");
   }

  // these are MPCI for Farm calculations 
   //Step2
   calculateMPCIforCropandfarm(cropid:number,APH:number,practice:string,Acres:number,countyid:number):number{
    let insurancepoliciesforfarm=this.localloanobject.InsurancePolicies.filter(p=>p.Practice==practice && p.CropId==cropid && p.CountyId==countyid);
    if(insurancepoliciesforfarm.length>0){
      let policy=insurancepoliciesforfarm[0];
      if(policy.Type==InsuranceType.ARH){
          return (policy.RevenueFactor * policy.AnnualRevenue * policy.PaymentFactor * (policy.Level/100)) * Acres;
      }
      else{
        return (((policy.Level/100) * policy.Price * APH ) - policy.Premium)*Acres;
      }
    }
   }
   //Step1
   calculateMPCISumCollateral(LoanId:number):Array<CropMPCI>{
    //First lets get the policies 
     this.localloanobject=(<Loan>this.localst.retrieve(environment.loankey));
     let mpciobj:Array<CropMPCI>=new Array<CropMPCI>();
     if(this.localloanobject.InsurancePolicies.length!=0){
        this.localloanobject.Farms.sort(p=>p.Id).forEach((elementmain,index) => {
            let farmunit=this.localloanobject.FarmUnits.filter(p=>p.FarmId==elementmain.Id && p.Practice==elementmain.Practice)[0];
            let FarmUnitId=farmunit.Id;
            let itercrops=this.localloanobject.CropUnits.filter(p=>p.FarmUnitId== FarmUnitId);
            itercrops.forEach(element => {
              //add column here for UI
              let obj:CropMPCI=new CropMPCI();
              obj.Acres=element.Acres;
              obj.CropId=element.CropId;
              obj.CropName=this.getcropname(obj.CropId);
              obj.FSNId=elementmain.FSN;
              obj.LoanId=elementmain.LoanId;
              obj.APH=element.APH;
              obj.Practice=elementmain.Practice;
              let insurancepoliciesforfarm=this.localloanobject.InsurancePolicies.filter(p=>p.Practice==elementmain.Practice && p.CropId==element.CropId && p.CountyId==elementmain.CountyId);
              if(insurancepoliciesforfarm.length>0){
              obj.Price=insurancepoliciesforfarm[0].Price;
              obj.Premium=insurancepoliciesforfarm[0].Premium;
              obj.Policylevel=insurancepoliciesforfarm[0].Level;
              }
              obj.MPCI=this.calculateMPCIforCropandfarm(element.CropId,element.APH,farmunit.Practice,element.Acres,elementmain.CountyId);
              if(obj.Acres!=0 && obj.Acres!=undefined && obj.APH!=0 && obj.APH!=undefined)
              mpciobj.push(obj);
            });
          });
          
          return mpciobj;
     }
   }
   getcropname(id:number):string{
    let rec=this.croptypes.find(p=>p.id==id);
    if(rec!=null){
      return  rec.value;
    }
  }
  //
}
