import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { observeOn } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { BalanceSheetCalculationService } from './balancesheetcalculation.service';
import { LoancropcalculationService } from './loancropcalculation.service';
import { Loan } from '../../models/appmodels/loan.model';
import { InsuranceCalculationService } from './insurancecalculation.service';
import { CollateralService } from './collateral.service';


@Injectable()
export class LoancalculationService {
  constructor(
    private localst: LocalStorageService,
    private balancesheetcalserv: BalanceSheetCalculationService,
    private loancropcalcservice: LoancropcalculationService,
    public insurancecalc: InsuranceCalculationService,
    private collateralService: CollateralService
  ) {}

  performcalculationonloanobject(localloanobj: Loan) {
    console.log(new Date().getMilliseconds());


    localloanobj.BalanceSheetItems = this.balancesheetcalserv.preparebalancesheetitemfromArray(localloanobj.BalanceSheetItems);
    localloanobj.IncomeRecords = this.balancesheetcalserv.prepareincomehistoryitemfromArray(localloanobj.IncomeRecords);
    localloanobj.Rebates = localloanobj.Rebates != null ? this.loancropcalcservice.preparerebatesitemfromArray(localloanobj.Rebates) : null;
    localloanobj.LoanCrops = this.loancropcalcservice.prepareloancropitemfromArray(localloanobj.LoanCrops);
    localloanobj.FC_MPCIValues = this.insurancecalc.calculateMPCISumCollateral(localloanobj.Id);
    localloanobj.CollateralItems =this.collateralService.prepareCollateralItemsFromArray(localloanobj.CollateralItems);
    debugger
      // At End push the new obj with new caluclated values into localstorage and emit value changes
      this.localst.store(environment.loankey, localloanobj);

    console.log("object updated");
    console.log(new Date().getMilliseconds());
  }

}
