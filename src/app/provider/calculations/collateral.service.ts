import { Injectable } from '@angular/core';

@Injectable()
export class CollateralService {

  constructor() { }

  calculateDiscountValue(data) {
    debugger
    if(data.PriorLien!=null)
    data.DiscountValue = (data.MarketValue * (1 - (data.Discount/100))) - data.PriorLien
    else
    data.DiscountValue = data.MarketValue * (1 - data.Discount)
  }

  prepareCollateralItemsFromArray(collateralItems: Array<any>) {
    if(collateralItems!=null && collateralItems!=undefined)
    {
    collateralItems.forEach(element => {
      element = this.calculateDiscountValue(element);
    });
  }
    return collateralItems;
  }
}
