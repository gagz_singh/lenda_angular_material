import { Injectable } from '@angular/core';
import { BalanceSheetItemsEntity } from '../../models/appmodels/loan.model';

@Injectable()
export class BalanceSheetCalculationService {

  constructor() { }

  calculateAdjustedValue(assets: number, discount: number): number {
    return Math.round((assets - ((assets * discount) / 100)));
  }

  calculateAdjustedNetWorth(adjustedvalue: number, liablity: number): number {
    return Math.round((adjustedvalue - liablity));
  }

  calculateNetIncome(Revenue: number, Expenses: number): number {
    return Math.round((Revenue - Expenses));
  }

  //this method is for preparing balancesheetitem model after all caluclations
  preparebalancesheetitem(loanobj: BalanceSheetItemsEntity): any { ///expexts single object not a list
    //step 1 as adjustednetworth depends on adjusted value
    loanobj.AdjustedValue = this.calculateAdjustedValue(loanobj.Assets, loanobj.Discount);
    loanobj.AdjustedNetWorth = this.calculateAdjustedNetWorth(loanobj.AdjustedValue, loanobj.Liability);
    loanobj.FC_Equity = this.calculateEquity(loanobj.Assets, loanobj.Liability);
    if (loanobj.Name == "Current")
      loanobj.FC_Ratios = this.calculateRatios(loanobj.Assets, loanobj.Liability);
    return loanobj;
  }
  //same method as above but converts array ... for calculation service
  preparebalancesheetitemfromArray(loanobj: Array<any>): Array<any> {
    loanobj.forEach(element => {
      element = this.preparebalancesheetitem(element);
    });
    return loanobj;
  }

  prepareincomehistoryitem(loanobj: any): any {
    loanobj.Income = this.calculateNetIncome(loanobj.Revenue, loanobj.Expenses);
  }

  prepareincomehistoryitemfromArray(loanobj: Array<any>): Array<any> {
    loanobj.forEach(element => {
      element = this.prepareincomehistoryitem(element);
    });
    return loanobj;
  }
  calculateEquity(Assets: number, liablity: number): number {
    return Math.round((Assets - liablity));
  }

  calculateRatios(Assets: number, Liability: number): number {
    return Math.round((Assets / Liability) * 100);
  }

}
