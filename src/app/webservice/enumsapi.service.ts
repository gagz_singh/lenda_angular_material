import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../models/response.model';
import { ApiService } from '../services';

@Injectable()
export class EnumsapiService {

  constructor(private apiservice: ApiService) { }

  getcroptypesenum() : Observable<ResponseModel>{
    const route='/api/Enums/GetCroptypes';
   return this.apiservice.get(route).map(res=>res);
   }
}
