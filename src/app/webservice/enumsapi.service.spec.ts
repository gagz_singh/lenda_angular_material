import { TestBed, inject } from '@angular/core/testing';

import { EnumsapiService } from './enumsapi.service';

describe('EnumsapiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnumsapiService]
    });
  });

  it('should be created', inject([EnumsapiService], (service: EnumsapiService) => {
    expect(service).toBeTruthy();
  }));
});
