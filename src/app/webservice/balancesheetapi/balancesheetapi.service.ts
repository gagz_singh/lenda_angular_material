import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../../models/test.models';
import { ApiService } from '../../services/api.service';
const API_URL = environment.apiUrl;

@Injectable()
export class BalanceSheetApiService {

  
  constructor( private apiservice:ApiService) { }
  getRecordsByloanId(loanid:number) : Observable<ResponseModel>{
    const route='/api/BalanceSheet/GetbyLoanId/'+loanid;
   return this.apiservice.get(route).map(res=>res);
   }

   getIncomeRecordsByloanId(loanid:number) : Observable<ResponseModel>{
    const route='/api/IncomeHistory/GetIncomeRecordbyLoanId/'+loanid;
   return this.apiservice.get(route).map(res=>res);
   }

}
