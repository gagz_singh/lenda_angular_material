import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../../models/test.models';
import { environment } from '../../../environments/environment';
import { ApiService } from '../../services/api.service';
const API_URL = environment.apiUrl;

@Injectable()
export class MasterApiService {

    constructor(private apiservice: ApiService) { }

    getLoanStatusList(params): Observable<ResponseModel> {
            
        const route = '/api/Master/GetLoanStatusList/'+ params.pageSize+'/'+ params.prevIndex;
        return this.apiservice.get(route).map(res => res);
    }


    addEditLoanStatus(params): Observable<ResponseModel> {
            
        const route = '/api/Master/AddEditLoanStatus';
        return this.apiservice.post(route, params).map(res => res);
    }

    deleteLoanStatus(params): Observable<ResponseModel> {
        const route = '/api/Master/DeleteLoanStatus/' + params;
            return this.apiservice.get(route, params).map(res => res);
        }

        addEditQuestionCategory(params): Observable<ResponseModel> {
            const route = '/api/Master/addEditQuestionCategory';
            return this.apiservice.post(route, params).map(res => res);
        }

        deleteQuestionCategory(params): Observable<ResponseModel> {
        const route = '/api/master/deleteQuestionCategory';
        return this.apiservice.get(route, params).map(res => res);
    }
}
