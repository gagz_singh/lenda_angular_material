import { NgModule, Component } from "@angular/core";
import { RouterModule, Routes, RouterLink, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationEnd } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { AboutComponent } from "./about/about.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { AppComponent } from "./app.component";
import { MasterComponent } from "./master/master.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoanStatusComponent } from './components/master/loan-status/loan-status.component';
import { LoanStatusListComponent } from './components/master/loan-status-list/loan-status-list.component';
import { LoanListComponent } from "./components/home/loan-list/loan-list.component";
import { LoanoverviewComponent } from "./components/loanoverview/loanoverview.component";
import { CroproductionhistoryComponent } from "./components/loanoverview/crops/croproductionhistory/croproductionhistory.component";
import { CropsComponent } from "./components/loanoverview/crops/crops.component";
import { borrowerComponent } from "./components/loanoverview/borrower/borrower.component";
import { QuestionCategoriesComponent } from "./components/master/question-categories/question-categories.component";
import { OptimizerComponent } from "./components/loanoverview/optimizer/optimizer.component";
import { CrossCollateralComponent } from "./components/loanoverview/collateral/cross-collateral/cross-collateral.component";
import { CollateralComponent } from "./components/loanoverview/collateral/collateral.component";
import { DebugpageComponent } from "./components/loanoverview/debugpage/debugpage.component";
import { LoanObjectResolver } from "./resolver/loanresolver";
import { LoanobjectpreviewComponent } from "./components/loanoverview/loanobjectpreview/loanobjectpreview.component";
import { SummaryComponent } from "./components/loanoverview/summary/summary.component";
import { NamingConventionComponent } from "./components/naming-convention/naming-convention.component";


const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: MasterComponent,
    children: [
      { path: "", redirectTo: "loanlist", pathMatch: "full" },
      { path: 'loanlist', component: LoanListComponent },
      { path: 'loanpreview', component: LoanobjectpreviewComponent },
      {
        path: 'loanoverview',
        component: LoanoverviewComponent, 
        resolve:{
        loandata:LoanObjectResolver
        },
       children:
          [
            { path: 'crops/:id', component: CropsComponent },
            { path: 'borrower/:id', component: borrowerComponent },
            { path: 'optimizer/:id', component: OptimizerComponent },
            { path: 'collaterals/:id', component: CollateralComponent },
            { path: 'debugpage/:id', component: DebugpageComponent },
            { path: 'collaterals/:id', component: CollateralComponent },
            { path: 'summary/:id', component: SummaryComponent }
          ]
      },
      { path: 'admin/loanstatus', component: LoanStatusComponent },
      { path: 'admin/getloanstatus', component: LoanStatusListComponent },
      { path: 'admin/getquestioncategory', component: QuestionCategoriesComponent },
      { path: 'admin/namingconventions', component: NamingConventionComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'about', component: AboutComponent },
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
