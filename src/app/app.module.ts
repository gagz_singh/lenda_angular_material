import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Http, HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastModule } from 'ng2-toastr';
import { LoadingModule } from 'ngx-loading';
//START MATERIAL COMPONENTS 
import { MatButtonModule, MatCheckboxModule, MatNativeDateModule, MAT_DATE_LOCALE, MatProgressSpinner, MatProgressSpinnerModule, MatTooltipModule, MatChipsModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule, MatSnackBarModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

//ENDS MATERIAL COMPONENTS

//START SERVICES 

import { GlobalService, MasterService, ApiService, MasterApiService } from './services';
import { SharedModule } from './shared/shared.module';
import { AlertifyService } from './alertify/alertify.service';

//END SERVICES 

//START COMPONENTS 
import { AppComponent } from './app.component';
import { AlertComponent, ConfirmComponent } from './alertify/components'
import { HeaderComponent } from './shared/layout/header.component';
import { FooterComponent } from './shared/layout/footer.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutComponent } from './about/about.component';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MasterComponent } from "./master/master.component";
import { QuestionCategoriesComponent } from './components/master/question-categories/question-categories.component';
import { QuestionTypeComponent } from './components/master/question-type/question-type.component';
import { LoanStatusComponent } from './components/master/loan-status/loan-status.component';
import { LoanStatusListComponent } from './components/master/loan-status-list/loan-status-list.component';
import { LoanListComponent } from './components/home/loan-list/loan-list.component';
import { LoanApiService } from './webservice/loanapi/loanapi.service';
import { LoanoverviewComponent } from './components/loanoverview/loanoverview.component';
import { CropsComponent } from './components/loanoverview/crops/crops.component';
import { CroproductionhistoryComponent } from './components/loanoverview/crops/croproductionhistory/croproductionhistory.component';
import { LocalStorageService } from 'ngx-webstorage';
import { IncomehistoryComponent } from './components/loanoverview/borrower/incomehistory/incomehistory.component';
import { borrowerComponent } from './components/loanoverview/borrower/borrower.component';
import { BalancesheetitemComponent } from './components/loanoverview/borrower/balancesheetitem/balancesheetitem.component';
import { BalanceSheetCalculationService } from './provider/calculations/balancesheetcalculation.service';
import { LoancropcalculationService } from './provider/calculations/loancropcalculation.service';
import { EnumsapiService } from './webservice/enumsapi.service';
import { AppService } from './services/appinitializeservice/app.service';
import { RebateentitiesComponent } from './components/loanoverview/crops/rebateentities/rebateentities.component';
import { BookingsComponent } from './components/loanoverview/crops/bookings/bookings.component';
import { RebatesComponent } from './components/loanoverview/crops/rebates/rebates.component';
import { LoancalculationService } from './provider/calculations/loancalculation.service';
import { QuestionCategoryModalComponent } from './components/master/question-category-modal/question-category-modal.component';
import { IndirectCropIncomeComponent } from './components/loanoverview/crops/indirect-crop-income/indirect-crop-income.component';
import { OptimizerComponent } from './components/loanoverview/optimizer/optimizer.component';
import { OptimizerIrComponent } from './components/loanoverview/optimizer/optimizer-ir/optimizer-ir.component';
import { AgGridModule } from 'ag-grid-angular';
import { NumericEditor } from "../app/aggridfilters/numericaggrid";
import { CrossCollateralComponent } from './components/loanoverview/collateral/cross-collateral/cross-collateral.component';
import { OtherCollateralComponent } from './components/loanoverview/collateral/other-collateral/other-collateral.component';
import { CollateralComponent } from './components/loanoverview/collateral/collateral.component';
import { DebugpageComponent } from './components/loanoverview/debugpage/debugpage.component';
import { FsaComponent } from './components/loanoverview/collateral/fsa/fsa.component';
import { StoredCropComponent } from './components/loanoverview/collateral/stored-crop/stored-crop.component';
import { EquipmentComponent } from './components/loanoverview/collateral/equipment/equipment.component';
import { RealEstateComponent } from './components/loanoverview/collateral/real-estate/real-estate.component';
import { InsuranceCalculationService } from './provider/calculations/insurancecalculation.service';
import { LoanObjectResolver } from './resolver/loanresolver';
import { OptimizerNiComponent } from './components/loanoverview/optimizer/optimizer-ni/optimizer-ni.component';
import { LoanobjectpreviewComponent } from './components/loanoverview/loanobjectpreview/loanobjectpreview.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { CollateralService } from './provider/calculations/collateral.service';
import { SummaryComponent } from './components/loanoverview/summary/summary.component';
import { AddCollateralModalComponent } from './components/loanoverview/collateral/add-collateral-modal/add-collateral-modal.component';
import { FinancialSnapshotComponent } from './components/loanoverview/summary/financial-snapshot/financial-snapshot.component';
import { NamingConventionComponent } from './components/naming-convention/naming-convention.component';
import { AggridTxtAreaComponent } from './aggridfilters/textarea';



//ENDS COMPONENTS
//APP INITIALIZATION


//

@NgModule({
  declarations: [
    NumericEditor,
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    DashboardComponent,
    AboutComponent,
    MasterComponent,
    AlertComponent,
    ConfirmComponent,
    FooterComponent,
    HeaderComponent,
    LoanStatusComponent,
    LoanStatusListComponent,
    QuestionCategoriesComponent,
    QuestionTypeComponent,
    LoanListComponent,
    LoanoverviewComponent,
    CropsComponent,
    CroproductionhistoryComponent,
    borrowerComponent,
    BalancesheetitemComponent,
    IncomehistoryComponent,
    RebateentitiesComponent,
    BookingsComponent,
    RebatesComponent,
    QuestionCategoryModalComponent,
    OptimizerComponent,
    OptimizerIrComponent,
    IndirectCropIncomeComponent,
    CrossCollateralComponent,
    OtherCollateralComponent,
    CollateralComponent,
    DebugpageComponent,
    OptimizerNiComponent,
    LoanobjectpreviewComponent,
    FsaComponent,
    StoredCropComponent,
    EquipmentComponent,
    RealEstateComponent,
    DebugpageComponent,
    SummaryComponent,
    AddCollateralModalComponent,
    FinancialSnapshotComponent,
    NamingConventionComponent,
    AggridTxtAreaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatFormFieldModule,
    MatMenuModule,
    MatToolbarModule,
    MatListModule,
    MatTableModule,
    MatGridListModule,
    MatDialogModule,
    MatExpansionModule,
    MatTabsModule,
    MatTooltipModule,
    MatIconModule,
    MatCardModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatButtonToggleModule,
    NgxDatatableModule,
    NgxJsonViewerModule,
    LoadingModule,
    AgGridModule.withComponents([NumericEditor]),
    ToastModule.forRoot()
  ],
  exports: [
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatFormFieldModule,
    MatMenuModule,
    MatToolbarModule,
    MatTooltipModule,
    MatListModule,
    MatTableModule,
    MatGridListModule,
    MatExpansionModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatButtonToggleModule,
  ],
  entryComponents: [
    LoanStatusComponent,
    QuestionCategoryModalComponent,
    AlertComponent,
    ConfirmComponent,
    AddCollateralModalComponent,
    AggridTxtAreaComponent
  ],
  providers: [
    GlobalService,
    MatDialogModule,
    MasterService,
    AlertifyService,
    ApiService,
    LocalStorageService,
    MasterApiService,
    LoanApiService,
    BalanceSheetCalculationService,
    LoancropcalculationService,
    EnumsapiService,
    LoancalculationService,
    AppService,
    InsuranceCalculationService,
    LoanObjectResolver,
    CollateralService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
