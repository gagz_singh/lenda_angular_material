import { Component, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { AppService } from './services/appinitializeservice/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Lenda';
  
  constructor(private appser:AppService, private toaster: ToastsManager, vcf: ViewContainerRef) {
    this.toaster.setRootViewContainerRef(vcf);
  
  }
}
