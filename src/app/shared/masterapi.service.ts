import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../models/test.models';
import { environment } from '../../environments/environment';
import { ApiService } from './../services/api.service';
const API_URL = environment.apiUrl;

@Injectable()
export class MasterApiService {

    constructor(private apiservice: ApiService) { }

    getLoanStatusList(params): Observable<ResponseModel> {

        const route = '/api/Master/GetLoanStatusList?pagesize=' + params.pageSize + '&previndex=' + params.prevIndex;
        return this.apiservice.get(route).map(res => res);
    }

    getIconList(): Observable<ResponseModel> {
        const route = '/api/Master/GetIconList';
        return this.apiservice.get(route).map(res => res);
    }


    addEditLoanStatus(params): Observable<ResponseModel> {
     debugger
        const route = '/api/Master/AddEditLoanStatus';
        return this.apiservice.post(route, params).map(res => res);
    }

    deleteLoanStatus(params): Observable<ResponseModel> {
        const route = '/api/Master/DeleteLoanStatus?loanStatusId=' + params;
        return this.apiservice.get(route, params).map(res => res);
    }

    getQuestionCategoryList(): Observable<ResponseModel> {

        const route = '/api/Master/GetQuestionCategoryList';
        return this.apiservice.get(route).map(res => res);
    }

    addEditQuestionCategory(params): Observable<ResponseModel> {
        const route = '/api/Master/AddEditQuestionCategory';
        return this.apiservice.post(route, params).map(res => res);
    }

    deleteQuestionCategory(params): Observable<ResponseModel> {
        const route = '/api/Master/DeleteQuestionCategory';
        return this.apiservice.get(route, params).map(res => res);
    }
}
