import {JsonObject, JsonProperty} from "json2typescript";
@JsonObject
export class UB_CrossCollateralsModel{

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;

    @JsonProperty("CrossId", Number)
    CrossId: number = undefined;

    @JsonProperty("CrossDescriptor", String)
    CrossDescriptor: string = undefined;

}