import {JsonObject, JsonProperty} from "json2typescript";
@JsonObject
export class UB_BalanceSheetItemModel{

    @JsonProperty("Assets", Number,true)
    Assets: number = undefined;

    @JsonProperty("Discount", Number,true)
    Discount: number = undefined;

    @JsonProperty("Liability", Number,true)
    Liability: number = undefined;

    @JsonProperty("Name", String)
    Type: string = undefined;

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;
    
    @JsonProperty("Id", Number)
    Id: number = undefined;

    @JsonProperty("AdjustedValue", Number,true)
    AdjustedValue: number = 0;

    @JsonProperty("AdjustedNetWorth", Number,true)
    AdjustedNetWorth: number = 0;
}

@JsonObject
export class UB_IncomeRecordModel{

    @JsonProperty("Year", Number)
    Year: number = undefined;

    @JsonProperty("Revenue", Number)
    Revenue: number = undefined;

    @JsonProperty("Expenses", Number)
    Expenses: number = undefined;

    @JsonProperty("Income", Number,true)
    Income: number = undefined;
    
    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;
    
    @JsonProperty("Id", Number)
    Id: number = undefined;

}