import { JsonObject, JsonProperty } from "json2typescript";
@JsonObject
export class UB_OtherCollateralsModel {
    
    @JsonProperty("Name", String)
    Name: string = undefined;

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;

    @JsonProperty("Type", Number)
    Type: number = undefined;

    @JsonProperty("Holder", Number)
    Holder: number = undefined;

    @JsonProperty("MarketValue", Number)
    MarketValue: number = undefined;

    @JsonProperty("Discount", Number)
    Discount: number = undefined;

    @JsonProperty("PriorLien", Number)
    PriorLien: number = undefined;

}