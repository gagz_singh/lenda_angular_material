import {JsonObject, JsonProperty, JsonConverter, JsonCustomConvert} from "json2typescript";


@JsonConverter
class StringConvertor implements JsonCustomConvert<string> {
    serialize(data: string): string {
        if(data==null){
            return "";
        }
        else
          return String(data);
    }
    deserialize(data: string): string {
      if(data==null){
          return "";
      }
      else
        return String(data);
    }
}

@JsonConverter
class IntConvertor implements JsonCustomConvert<number> {
    serialize(data: any): number {
        if(data==null){
            return 0;
        }
        else
          return Number(data);
    }
    deserialize(data: any): number {
      if(data==null){
          return 0;
      }
      else
        return Number(data);
    }
}

@JsonObject
export class UB_CropProductionHistoryModel{

    @JsonProperty("CropId", Number)
    CropId: number = undefined;

    @JsonProperty("SalePrice", Number)
    Price: number = undefined;

    @JsonProperty("ProductionHistory1", IntConvertor)
    ProductionHistory1: number = undefined;

    @JsonProperty("ProductionHistory2", IntConvertor)
    ProductionHistory2: number = undefined;

    @JsonProperty("ProductionHistory3", IntConvertor)
    ProductionHistory3: number = undefined;

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;
    
    @JsonProperty("Id", Number)
    Id: number = undefined;

    @JsonProperty("ProductionHistory4", IntConvertor)
    ProductionHistory4: number = 0;

    @JsonProperty("ProductionHistory5", IntConvertor)
    ProductionHistory5: number = 0;

    @JsonProperty("ProductionHistory6", IntConvertor)
    ProductionHistory6: number = 0;

    @JsonProperty("3yrAVG", Number,true)
    ThreeyrAVG: number = 0;

    @JsonProperty("6yrAVG", Number,true)
    SixyrAVG: number = 0;

    @JsonProperty("APH", Number,true)
    APH: number = 0;
}

@JsonObject
export class UB_BuyerRebateModel{

    @JsonProperty("Contact", StringConvertor,true)
    Contact: string = undefined;

    @JsonProperty("Email", StringConvertor , true)
    Email: string = undefined;

    @JsonProperty("Id", Number)
    Id: number = undefined;

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;

    @JsonProperty("Location", String,true)
    Location: string = undefined;

    @JsonProperty("Name", StringConvertor,true)
    buyerrebator: string = undefined;
    
    @JsonProperty("Phone", StringConvertor,true)
    Phone: string = undefined;

   
}


@JsonObject
export class UB_BookingModel{

    @JsonProperty("BuyerId", Number)
    BuyerId: number = undefined;

    @JsonProperty("CropId", Number )
    CropId: number = undefined;

    @JsonProperty("Id", Number)
    Id: number = undefined;

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;

    @JsonProperty("Price", Number)
    Price: number = undefined;

    @JsonProperty("Quantity", Number,true)
    Quantity: number = undefined;
   
}


@JsonObject
export class UB_RebateModel{

    @JsonProperty("RebatorId", Number)
    RebatorId: number = undefined;

    @JsonProperty("CropId", Number )
    CropId: number = undefined;

    @JsonProperty("Id", Number)
    Id: number = undefined;

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;

    @JsonProperty("Amount", Number)
    Amount: number = undefined;
   
}

