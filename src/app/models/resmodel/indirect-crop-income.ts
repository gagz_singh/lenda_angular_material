import {JsonObject, JsonProperty} from "json2typescript";
@JsonObject
export class UB_IndirectCropIncomeModel{

    @JsonProperty("LoanId", Number)
    LoanId: number = undefined;

    @JsonProperty("Source", String)
    Source: string = undefined;

    @JsonProperty("Description", String)
    Description: string = undefined;

    @JsonProperty("Amount", Number)
    Amount: number = undefined;

}