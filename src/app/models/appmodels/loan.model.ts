export interface Loan {
    Descriptor?: null;
    Properties?: null;
    LoanStatus?: null;
    LoanAgent?: null;
    Applicant?: null;
    Farmer?: null;
    DistributorContact?: null;
    InsuranceAgents?: null;
    Questions?: null;
    Financials?: null;
    IncomeRecords?: (IncomeRecordsEntity)[] | null;
    BalanceSheetItems?: (BalanceSheetItemsEntity)[] | null;
    Farms?: (Farm)[] | null;
    FarmUnits?: (FarmUnit)[] | null;
    LoanCrops?: (LoanCropsEntity)[] | null;
    CropUnits?: (CropUnit)[] | null;
    CommitteeMembers?: null;
    NotifiedParties?: null;
    Comments?: null;
    CropExpenses?: null;
    FarmExpenses?: null;
    LoanConditions?: null;
    CollateralItems?: (CollateralEntity)[] | null;
    Harvesters?: null;
    ThirdPartyCreditors?: null;
    References?: null;
    Lienholders?: null;
    IndirectIncomes?: null;
    CrossCollaterals?: null;
    InsurancePolicies?: (InsurancePolicies)[] |null;
    HistoricallyCalculatedValues?: null;
    HistoricallyCalculatedValuesSet?: null;
    IsStale: boolean;
    Rebates?: (RebatesEntity)[] | null;
    Bookings?: null;
    ContractEntities?: (ContractEntitiesEntity)[] | null;
    Affiliations?: null;
    PrerequisiteDocuments?: null;
    TransactionalDocuments?: null;
    WholeFarmPolicy?: null;
    Id?: null;
    FC_MPCIValues:(CropMPCI)[] | null;
  }
  export interface IncomeRecordsEntity {
    LoanId: number;
    Year: number;
    Revenue?: null;
    Expenses?: null;
    Income: number;
    Id: number;
  }
  export interface BalanceSheetItemsEntity {
    LoanId: number;
    Assets: number;
    Discount: number;
    Liability: number;
    AdjustedValue: number;
    AdjustedNetWorth: number;
    Name: string;
    Id: number;
    FC_Equity:number
    FC_Ratios:number
    FC_Fico:number;
    Rating:number;
  }
  export interface LoanCropsEntity {
    LoanId: number;
    CropId: number;
    SalePrice: number;
    ProductionHistory1: number;
    ProductionHistory2: number;
    ProductionHistory3: number;
    ProductionHistory4: number;
    ProductionHistory5: number;
    ProductionHistory6?: number | null;
    Id: number;
    CropName:string;
    Avg6:number;
    Avg3:number;
    APH:number;
  }
  export interface IndirectCropIncomeEntity {
    LoanId: number;
    Source: string;
    Description: string;
    Amount: number;
  }

  export interface RebatesEntity {
    CropName: string;
    LoanId: number;
    CropId: number;
    Amount: number;
    RebatorId?: null;
    Id: number;
  }
  
  export interface ContractEntitiesEntity {
    Bookings?: null;
    Rebates?: null;
    Contact?: string | null;
    LoanId: number;
    Location: string;
    Phone?: string | null;
    Email?: null;
    Name: string;
    Id: number;
  }

  export interface Farm {
    LoanId: number;
    FSN: string;
    CountyId: number;
    StateId: number;
    Owner: string;
    Owned: boolean;
    IR: number;
    NI: number;
    RentShare: number;
    RentCash?: any;
    RentFullFarm?: any;
    RentDue?: any;
    RentWaived?: any;
    PermissionToInsure: boolean;
    RentPaid: boolean;
    Acres: number;
    Practice: string;
    CountyName: string;
    StateName:string;
    Id: number;
}

export interface FarmUnit {
  FarmId: number;
  Practice: string;
  Acres: number;
  Id: number;
}

export interface CropUnit {
  FarmUnitId: number;
  LoanCropId: number;
  APH: number;
  Acres: number;
  ShareOverride: number;
  Id: number;
  CropName:string
  CropId:number
}

export interface InsurancePolicies {
  LoanId: number;
  AgentId: number;
  CountyId: number;
  CropId: number;
  ProviderId: number;
  Practice: string;
  Type: number;
  Unit: number;
  Options?: any;
  Level: number;
  Price: number;
  AnnualRevenue?: any;
  RevenueFactor?: any;
  PaymentFactor?: any;
  Premium: number;
  UseHmax: boolean;
  HmaxLevel?: any;
  HmaxPrice?: any;
  HmaxPremium?: any;
  HmaxLateDeductible?: any;
  HmaxMinimumDeductiblePercent?: any;
  HmaxMinimumDeductible?: any;
  UseStax: boolean;
  StaxLossTrigger?: any;
  StaxCoverageRange?: any;
  StaxProtectionFactor?: any;
  StaxExpectedYield?: any;
  UseSco: boolean;
  ScoExpectedYield?: any;
  CountyName: string;
  StateName: string;
  AgentName: string;
  CropName: string;
  Id: number;
}

export class CropMPCI {
  CropName: string;
  LoanId: number;
  CropId: number;
  MPCI: number;
  Acres?: number;
  FSNId: string;
  APH :number;
  Practice:string;
  Price:number;
  Premium:number;
  Policylevel:number

}
  
export interface CollateralEntity {
  LoanId: number;
  Type: number;
  Holder: string;
  MarketValue: number;
  Discount: number;
  DiscountValue: number;
  PriorLien : number;
  Name: string;
  
}