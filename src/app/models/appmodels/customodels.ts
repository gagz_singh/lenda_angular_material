export class NamingConventionEntity {
    Id:number;
    UI_Table_Name: string;
    DB_Table_Name: string;
    LoanObject: string;
    Level: string;
    LendaPlusName: string;
    Comments:string;
}