 export enum InsuranceType {
        RP = 1,
        RPHPE,
        YP,
        CAT,
        ARH
    }