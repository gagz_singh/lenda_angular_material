export class LoanStatusReqModel {
    public Id: number = null;
    public LoanStatusName: string;
    public SortOrder: string;
    public IconId: number;
    public Status: boolean;
}