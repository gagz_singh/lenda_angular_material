export class CollateralReqModel {
    public LoanId: number;
    public Type: number;
    public Holder: string;
    public MarketValue: number;
    public Discount: number;
    public PriorLien: number;
    public Name: string;
}