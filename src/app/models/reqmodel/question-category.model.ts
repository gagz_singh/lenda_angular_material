export class QuestionCategoryReqModel {
    public TabId: number;
    public ChevronId: number;
    public QuestionCategoryTypeName: string;
    public IconCode: number;
    public SortOrder: string;
    public Status: boolean;
}