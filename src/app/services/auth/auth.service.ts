import { Injectable } from '@angular/core';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt'
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';



@Injectable()
export class AuthService {
  public token: string;
  constructor(
    private http: Http,
    public jwtHelper: JwtHelper,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {

    // set token if saved in local storage
    var currentUser = JSON.parse(this.localStorageService.retrieve('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  // ...
  public isAuthenticated(): boolean {
    debugger
    var currentUser = JSON.parse(this.localStorageService.retrieve('currentUser'));
    this.token = currentUser && currentUser.token;
    //const token = this.persistenceService.get('token',StorageType.LOCAL);
    if (this.token == undefined) {
      return false;
    }
    // Check whether the token is expired and return
    // true or false
    //return !this.jwtHelper.isTokenExpired(this.token);

    return true;
  }


  loggedIn() {
    return tokenNotExpired();
  }


  login(username: string, password: string): Observable<boolean> {
    return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
      .map((response: Response) => {
        debugger
        // login successful if there's a jwt token in the response
        let token = response.json() && response.json().token;
        if (token) {
          // set token property
          this.token = token;

          // store username and jwt token in local storage to keep user logged in between page refreshes
          this.localStorageService.store('currentUser', JSON.stringify({ username: username, token: token }));

          // return true to indicate successful login
          return true;
        } else {
          // return false to indicate failed login
          return false;
        }
      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    this.localStorageService.clear('currentUser');
    this.router.navigate(['/']);
  }
}