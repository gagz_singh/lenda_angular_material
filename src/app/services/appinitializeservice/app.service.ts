import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { EnumsapiService } from '../../webservice/enumsapi.service';

@Injectable()
export class AppService {

  constructor(localst:LocalStorageService,enumservice:EnumsapiService) {

    enumservice.getcroptypesenum().subscribe(res=>{
      localst.store('croptypes',res.Data);
    })
   }

}
