import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { LoanStatusReqModel } from '../../../models/reqmodel/loan-status.model';
import { MasterApiService } from '../../../services/masterapi.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IconResModel } from '../../../models/resmodel/icon.model';

@Component({
  selector: 'app-loan-status',
  templateUrl: './loan-status.component.html',
  styleUrls: ['./loan-status.component.scss']
})
export class LoanStatusComponent implements OnInit {
  loanModel: LoanStatusReqModel = new LoanStatusReqModel();
  isEdit: boolean = false;
  iconList: Array<IconResModel> = new Array<IconResModel>();
  //EventEmitter
  onNewLoanStatusDataRecieved = new EventEmitter();

  constructor(
    private router: Router,
    private toaster: ToastsManager,
    private masterApiService: MasterApiService,
    public dialogRef: MatDialogRef<LoanStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    debugger
    if (data != null) {
      this.isEdit = data.isEdit;
      if (this.isEdit) {
        debugger
        this.loanModel.Id = data.loanStatusModel.Id;
        this.loanModel.LoanStatusName = data.loanStatusModel.LoanStatusName;
        this.loanModel.SortOrder = data.loanStatusModel.SortOrder;
        this.loanModel.IconId = data.loanStatusModel.IconId;
        this.loanModel.Status = data.loanStatusModel.Status;
      }
      else {
        this.loanModel = new LoanStatusReqModel()
      }
    }
    else {
      this.toaster.error("Something went wrong");
    }
  }

  ngOnInit() {
    this.getIconList();
    this.loanModel.Status = true;
  }

  closeModal() {
    this.dialogRef.close();
  }

  // Function for getting iconlist
  getIconList() {
    this.masterApiService.getIconList().subscribe(res => {
      if (res.ResCode == 1) {
        this.iconList = res.Data;
        console.log(this.iconList);
      }
    });
  }

  
  // Function for adding/editing new loan status
  saveLoanStatus(form) {
    if (!form.invalid) {
   
      this.masterApiService.addEditLoanStatus(this.loanModel).subscribe(res => {
        if (res.ResCode == 1) {
          this.loanModel = new LoanStatusReqModel();
          this.closeModal();
          this.onNewLoanStatusDataRecieved.emit();
          //this.router.navigate(['/admin/getloanstatus']);
          this.toaster.success('Loan status saved successfully.');
        }
      })
    }
  }
}
