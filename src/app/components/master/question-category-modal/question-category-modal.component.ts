import { Component, OnInit, EventEmitter } from '@angular/core';
import { QuestionCategoryReqModel } from '../../../models/reqmodel/question-category.model';
import { MasterApiService } from '../../../services/masterapi.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-question-category-modal',
  templateUrl: './question-category-modal.component.html',
  styleUrls: ['./question-category-modal.component.scss']
})
export class QuestionCategoryModalComponent implements OnInit {
  onNewQuestionCategoryDataRecieved = new EventEmitter();
  questionCategoryModel: QuestionCategoryReqModel = new QuestionCategoryReqModel();
  constructor(private toaster: ToastsManager,
    private masterApiService: MasterApiService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<QuestionCategoryModalComponent>
  ) { }

  ngOnInit() {
  }

  // Function for adding/editing new loan status
  saveLoanStatus(form) {
    if (!form.invalid) {

      this.masterApiService.addEditLoanStatus(this.questionCategoryModel).subscribe(res => {
        if (res.ResCode == 1) {
          this.questionCategoryModel = new QuestionCategoryReqModel();
          this.closeModal();
          this.onNewQuestionCategoryDataRecieved.emit();
          //this.router.navigate(['/admin/getloanstatus']);
          this.toaster.success('Loan status saved successfully.');
        }
      })
    }
  }
  closeModal() {
    this.dialogRef.close();
  }
}
