import { Component, OnInit } from '@angular/core';
import { QuestionCategoryReqModel } from '../../../models/reqmodel/question-category.model';
import { ToastsManager } from 'ng2-toastr';
import { MasterApiService } from '../../../services/masterapi.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { QuestionCategoryModalComponent } from '../question-category-modal/question-category-modal.component';
@Component({
  selector: 'app-question-categories',
  templateUrl: './question-categories.component.html',
  styleUrls: ['./question-categories.component.scss']
})
export class QuestionCategoriesComponent implements OnInit {
  questionCategoryModel: QuestionCategoryReqModel = new QuestionCategoryReqModel();
  questionCategoryList: any;
  constructor(private toaster: ToastsManager, public dialogRef: MatDialogRef<QuestionCategoriesComponent>, private masterApiService: MasterApiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getQuestionCategoryList();
                          }

  // Function for opening add question's category dialog
  openAddQuestionCategoryModal(): void {
    const dialogRef = this.dialog.open(QuestionCategoryModalComponent, {
      height: 'auto',
      width: '770px',
      data: { isEdit: false, questionCategoryModel: null }
    });
    // const sub = dialogRef.componentInstance.onNewLoanStatusDataRecieved.subscribe(
    //   (data: any) => {
    //     this.getQuestionCategoryList();
    //   });
  }

  // Function for opening edit question's category dialog
  openEditLoanStatusModal(model) {
    const dialogRef = this.dialog.open(QuestionCategoryModalComponent, {
      height: 'auto',
      width: '770px',
      data: { isEdit: true, questionCategoryModel: model }
    });
    // const sub = dialogRef.componentInstance.onNewLoanStatusDataRecieved.subscribe(
    //   (data: any) => {
    //     this.getLoanStatusList(20, 0);
    //   });
  }

  // Function for getting loan's status list
  getQuestionCategoryList() {
    debugger
    this.masterApiService.getQuestionCategoryList().subscribe(res => {
      if (res.ResCode == 1) {
        this.questionCategoryList = res.Data;
        console.log(this.questionCategoryList);
        this.toaster.success('Question Category successfully retrieved.');
      }
    });
  }


  // Function for deleting loan's status 
  deleteLoanStatus(id) {
    this.masterApiService.deleteLoanStatus(id).subscribe(res => {
      if (res.ResCode == 1) {
        this.getQuestionCategoryList();
        this.toaster.success('Question Category deleted.');
      }
    });
  }
}
