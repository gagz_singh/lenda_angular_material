import { Component, OnInit, EventEmitter } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { MasterApiService } from '../../../services/masterapi.service';
import { Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { LoanStatusComponent } from '../loan-status/loan-status.component';
import { DropDownModel } from '../../../models/resmodel/common/dropdown.model';

@Component({
  selector: 'app-loan-status-list',
  templateUrl: './loan-status-list.component.html',
  styleUrls: ['./loan-status-list.component.scss']
})

export class LoanStatusListComponent implements OnInit {
  //EventEmitters
  onLoanStatusDelete = new EventEmitter();
  onLoanStatusEdit = new EventEmitter();
  loanStatusList: Array<any> = new Array<any>();


  constructor(
    private router: Router,
    private toaster: ToastsManager,
    private masterApiService: MasterApiService,
    public dialog: MatDialog,
  ) { }

  //On Component Initialize
  ngOnInit() {
    this.getLoanStatusList(20, 0);
  }


  // Function for opening add loan's status dialog
  openAddLoanStatusModal(): void {
    const dialogRef = this.dialog.open(LoanStatusComponent, {
      height: 'auto',
      width: '770px',
      data: { isEdit: false, loanStatusModel: null }
    });
    const sub = dialogRef.componentInstance.onNewLoanStatusDataRecieved.subscribe(
      (data: any) => {
        this.getLoanStatusList(20, 0);
      });
  }

  // Function for opening edit loan's status dialog
  openEditLoanStatusModal(model) {
    const dialogRef = this.dialog.open(LoanStatusComponent, {
      height: 'auto',
      width: '770px',
      data: { isEdit: true, loanStatusModel: model }
    });
    const sub = dialogRef.componentInstance.onNewLoanStatusDataRecieved.subscribe(
      (data: any) => {
        this.getLoanStatusList(20, 0);
      });
  }

  

  // Function for getting loan's status list
  getLoanStatusList(pageSize, prevIndex) {
    let params = { pageSize: pageSize, prevIndex: prevIndex }
    this.masterApiService.getLoanStatusList(params).subscribe(res => {
      if (res.ResCode == 1) {
        this.loanStatusList = res.Data;
        // console.log(this.loanStatusList);
        // this.toaster.success('Loan status successfully retrieved.');
      }
    });
  }


  // editLoanStatus(data) {
  //   this.router.navigate(['/home/loanstatus']);
  //   this.onLoanStatusEdit.emit(data)
  // }

   // Function for deleting loan's status 
  deleteLoanStatus(id) {
    this.masterApiService.deleteLoanStatus(id).subscribe(res => {
      if (res.ResCode == 1) {
        this.getLoanStatusList(20, 0);
        this.toaster.success('Loan status deleted.');
      }
    });
  }
}
