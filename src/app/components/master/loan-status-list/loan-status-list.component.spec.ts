import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanStatusListComponent } from './loan-status-list.component';

describe('LoanStatusListComponent', () => {
  let component: LoanStatusListComponent;
  let fixture: ComponentFixture<LoanStatusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanStatusListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanStatusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
