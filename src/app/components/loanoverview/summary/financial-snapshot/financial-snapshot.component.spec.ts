import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialSnapshotComponent } from './financial-snapshot.component';

describe('FinancialSnapshotComponent', () => {
  let component: FinancialSnapshotComponent;
  let fixture: ComponentFixture<FinancialSnapshotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialSnapshotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialSnapshotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
