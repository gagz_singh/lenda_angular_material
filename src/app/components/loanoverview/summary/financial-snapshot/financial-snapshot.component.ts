import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancalculationService } from '../../../../provider/calculations/loancalculation.service';
import { Loan } from '../../../../models/appmodels/loan.model';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-financial-snapshot',
  templateUrl: './financial-snapshot.component.html',
  styleUrls: ['./financial-snapshot.component.scss']
})
export class FinancialSnapshotComponent implements OnInit {
  private gridApi;
  private gridColumnApi;
  public columnDefs;
  public frameworkComponents;
  localLoanObj:Loan;
  rows=[];
  constructor(private localSt:LocalStorageService,private loancalulationservice:LoancalculationService) {
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rows=this.localLoanObj.BalanceSheetItems;
   }
  )};
  ngOnInit() {
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.columnDefs = [
          {
            headerName: "Type",
            field: "Name",
            width:150
          },
          {
            headerName: "Assets",
            field: "Assets",
           width:150
          },
          {
            headerName: "Liabilities",
            field: "Liability",
           width:150
          },
          {
            headerName: "Equity",
            field: "FC_Equity",
           width:150
          },
          {
            headerName: "Ratios",
            field: "FC_Ratios",
           width:150
          },
          {
            headerName: "FICO",
            field: "FC_Fico",
           width:150
          },
          {
            headerName: "Rating",
            field: "Rating",
           width:150
          }
          
    ];
    this.getFinancialSnapshots();
  }
  onGridReady(params) {
    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.rows);
   
  }
  getFinancialSnapshots(){
    this.rows=this.localLoanObj.BalanceSheetItems;
   }
   celleditingstopped(event:any){

   }
}
