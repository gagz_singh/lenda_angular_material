import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Loan } from '../../../../models/appmodels/loan.model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../../environments/environment';
import { LoancalculationService } from '../../../../provider/calculations/loancalculation.service';
import { HttpClient } from '@angular/common/http';
import { OptimizerIR } from '../../../../models/appmodels/uioptimizer.model';
import { ChildrenOutletContexts } from '@angular/router';
import { NumericEditor } from '../../../../aggridfilters/numericaggrid';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-optimizer-ir',
  templateUrl: './optimizer-ir.component.html',
  styleUrls: ['./optimizer-ir.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class OptimizerIrComponent implements OnInit {
  private gridApi;
  private gridColumnApi;
  public columnDefs;
  public frameworkComponents;
  localLoanObj:Loan;

  editing = {};
  rows=[];
  constructor(private localSt:LocalStorageService,private loancalulationservice:LoancalculationService) { 
   
    this.localSt.observe(environment.loankey).subscribe(res=>{
       this.localLoanObj=<Loan>res;
   })
  }
  onGridReady(params) {
    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.rows);
   
  }
  
  ngOnInit() {
    this.frameworkComponents = {
      numericEditor: NumericEditor
    };
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.columnDefs = [
      {
        headerName: "Farm",
        children: [
          {
            headerName: "State/County",
            field: "StateandCounty",
            width:150
          },
          {
            headerName: "FSN",
            field: "FSN",
           width:80
          },
          {
            headerName: "Prac",
            field: "Practice",
           width:80
          },
          {
            headerName: "Acres",
            field: "Acres",
           width:80
          }
        ]
      }
    ];
    this.getOptimizerIRitems();

  }

  getOptimizerIRitems(){
    this.localLoanObj.Farms.filter(p=>p.Practice=='IR').forEach((element,index) => {
      let obj:any={};
      obj.StateandCounty=element.StateName+"|"+element.CountyName;
      obj.FSN=element.FSN;
      obj.Practice=element.Practice;
      obj.Acres=element.Acres;
      obj.FarmRecordId=element.Id;
      obj.FarmUnitId=this.localLoanObj.FarmUnits.filter(p=>p.FarmId==element.Id && p.Practice=="IR")[0].Id;
      let itercrops=this.localLoanObj.CropUnits.filter(p=>p.FarmUnitId== obj.FarmUnitId);
      
      itercrops.forEach(element => {
        //add column here for UI
        if(index==0)
        {
       let header:any={};
       header.headerName=element.CropName;
       header.children=[];
       header.children.push({headerName:"ShareOverride",field:element.CropName+'_ShareOverride',width:80,editable:true, cellEditorFramework: NumericEditor})
       header.children.push({headerName:"Acres",field:element.CropName+'_Acres',width:80,editable:true, cellEditorFramework: NumericEditor})
       header.children.push({headerName:"APH",field:element.CropName+'_APH',width:80,editable:true, cellEditorFramework: NumericEditor})
       this.columnDefs.push(header);
        } 
      obj[element.CropName+'_Id']=element.Id;
      obj[element.CropName+'_Acres']=element.Acres;
      obj[element.CropName+'_APH']=element.APH;
      obj[element.CropName+'_ShareOverride']=element.ShareOverride;
      obj[element.CropName+'_FarmUnitId']=element.FarmUnitId;
      obj[element.CropName+'_LoanCropId']=element.LoanCropId;
      });
      
      this.rows.push(obj);
    });
    
   }
  
   celleditingstopped(event:any){
     debugger
     let changedcropname=event.column.colId.split('_')[0];
     let changedcropproperty=event.column.colId.split('_')[1];
     let changedcropid=event.data[changedcropname+"_Id"];
     this.localLoanObj.CropUnits.filter(p=>p.Id==changedcropid)[0][changedcropproperty]=event.value;
     this.loancalulationservice.performcalculationonloanobject(this.localLoanObj);
   }

}
