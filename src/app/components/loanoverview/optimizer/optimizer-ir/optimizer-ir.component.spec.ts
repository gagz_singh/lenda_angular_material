import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimizerIrComponent } from './optimizer-ir.component';

describe('OptimizerIrComponent', () => {
  let component: OptimizerIrComponent;
  let fixture: ComponentFixture<OptimizerIrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptimizerIrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizerIrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
