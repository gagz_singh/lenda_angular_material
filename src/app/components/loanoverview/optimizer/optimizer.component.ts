import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-optimizer',
  templateUrl: './optimizer.component.html',
  styleUrls: ['./optimizer.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class OptimizerComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

}
