import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimizerNiComponent } from './optimizer-ni.component';

describe('OptimizerNiComponent', () => {
  let component: OptimizerNiComponent;
  let fixture: ComponentFixture<OptimizerNiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptimizerNiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizerNiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
