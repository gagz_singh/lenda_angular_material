import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Loan } from '../../../../models/appmodels/loan.model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../../environments/environment';
import { LoancalculationService } from '../../../../provider/calculations/loancalculation.service';
import { HttpClient } from '@angular/common/http';
import { OptimizerIR } from '../../../../models/appmodels/uioptimizer.model';
import { ChildrenOutletContexts } from '@angular/router';
import { NumericEditor } from '../../../../aggridfilters/numericaggrid';

@Component({
  selector: 'app-optimizer-ni',
  templateUrl: './optimizer-ni.component.html',
  styleUrls: ['./optimizer-ni.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class OptimizerNiComponent implements OnInit {
  private gridApi;
  private gridColumnApi;
  public columnDefs;
  public frameworkComponents;
  localLoanObj:Loan;

  editing = {};
  rows=[];
  constructor(private localSt:LocalStorageService,private loancalulationservice:LoancalculationService) { 
   
    this.localSt.observe(environment.loankey).subscribe(res=>{
       this.localLoanObj=<Loan>res;
   })
  }
  onGridReady(params) {
    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.rows);
   
  }
  ngOnInit() {
    this.frameworkComponents = {
      numericEditor: NumericEditor
    };
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.columnDefs = [
      {
        headerName: "Farm",
        children: [
          {
            headerName: "State/County",
            field: "StateandCounty",
            width:150
          },
          {
            headerName: "FSN",
            field: "FSN",
           width:80
          },
          {
            headerName: "Prac",
            field: "Practice",
           width:80
          },
          {
            headerName: "Acres",
            field: "Acres",
           width:80
          }
        ]
      }
    ];
    this.getOptimizerNIitems();

  }

  getOptimizerNIitems(){
    this.localLoanObj.Farms.filter(p=>p.Practice=='NI').forEach((element,index) => {
      let obj:any={};
      obj.StateandCounty=element.StateName+"|"+element.CountyName;
      obj.FSN=element.FSN;
      obj.Practice=element.Practice;
      obj.Acres=element.Acres;
      obj.FarmRecordId=element.Id;
      obj.FarmUnitId=this.localLoanObj.FarmUnits.filter(p=>p.FarmId==element.Id && p.Practice=="NI")[0].Id;
      let itercrops=this.localLoanObj.CropUnits.filter(p=>p.FarmUnitId== obj.FarmUnitId);
      
      itercrops.forEach(element2 => {
        //add column here for UI
        if(index==0)
        {
       let header:any={};
       header.headerName=element2.CropName;
       header.children=[];
       header.children.push({headerName:"ShareOverride",field:element2.CropName+'_ShareOverride',width:80,editable:true, cellEditorFramework: NumericEditor})
       header.children.push({headerName:"Acres",field:element2.CropName+'_Acres',width:80,editable:true, cellEditorFramework: NumericEditor})
       header.children.push({headerName:"APH",field:element2.CropName+'_APH',width:80,editable:true, cellEditorFramework: NumericEditor})
       this.columnDefs.push(header);
        } 
      obj[element2.CropName+'_Id']=element2.Id;
      obj[element2.CropName+'_Acres']=element2.Acres;
      obj[element2.CropName+'_APH']=element2.APH;
      obj[element2.CropName+'_ShareOverride']=element2.ShareOverride;
      obj[element2.CropName+'_FarmUnitId']=element2.FarmUnitId;
      obj[element2.CropName+'_LoanCropId']=element2.LoanCropId;
      });
      this.rows.push(obj);
    });
    
   }
  
   celleditingstopped(event:any){
    
   }

}
