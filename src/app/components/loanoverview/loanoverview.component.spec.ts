import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanoverviewComponent } from './loanoverview.component';

describe('LoanoverviewComponent', () => {
  let component: LoanoverviewComponent;
  let fixture: ComponentFixture<LoanoverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanoverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanoverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
