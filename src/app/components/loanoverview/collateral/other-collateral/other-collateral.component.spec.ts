import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherCollateralComponent } from './other-collateral.component';

describe('OtherCollateralComponent', () => {
  let component: OtherCollateralComponent;
  let fixture: ComponentFixture<OtherCollateralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherCollateralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherCollateralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
