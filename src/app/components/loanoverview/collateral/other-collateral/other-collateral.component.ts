import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';
import { serializearray } from '../../../../provider/commonmethods/appserializer';
import { environment } from '../../../../../environments/environment';
import { UB_OtherCollateralsModel } from '../../../../models/resmodel/other-collaterals';
import { Loan } from '../../../../models/appmodels/loan.model';
import { LoancalculationService } from '../../../../provider/calculations/loancalculation.service';

@Component({
  selector: 'app-other-collateral',
  templateUrl: './other-collateral.component.html',
  styleUrls: ['./other-collateral.component.scss']
})
export class OtherCollateralComponent implements OnInit {
  localLoanObj: any;
  rows: Array<any> = new Array<any>();
  FSARows: Array<any> = new Array<any>();
  storedCropRows: Array<any> = new Array<any>();
  equipmentsRows: Array<any> = new Array<any>();
  editing = {};

  constructor(private localSt: LocalStorageService, private loancalulationservice: LoancalculationService) {
    this.localSt.observe(environment.loankey).subscribe(res => {
      debugger
      this.localLoanObj = <Loan>res;
      this.rows = this.localLoanObj.CollateralItems;
      this.rows = this.rows.filter(x => x.Type == 0);
    })
  }

  ngOnInit() {
    this.localLoanObj = <Loan>(this.localSt.retrieve(environment.loankey));
    // this.localLoanObj = this.localSt.retrieve(environment.loankey);
    //this.getCollaterals();
  }

  getCollaterals() {
    this.rows = this.localLoanObj.CollateralItems;
    this.rows = this.rows.filter(x => x.Type == 0);
  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
    this.localLoanObj.IncomeRecords = this.rows;
    this.loancalulationservice.performcalculationonloanobject(this.localLoanObj);
  }
}
