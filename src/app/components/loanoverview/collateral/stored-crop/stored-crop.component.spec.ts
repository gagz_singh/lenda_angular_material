import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoredCropComponent } from './stored-crop.component';

describe('StoredCropComponent', () => {
  let component: StoredCropComponent;
  let fixture: ComponentFixture<StoredCropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoredCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoredCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
