import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsaComponent } from './fsa.component';

describe('FsaComponent', () => {
  let component: FsaComponent;
  let fixture: ComponentFixture<FsaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
