import { Component, OnInit } from '@angular/core';
import { AddCollateralModalComponent } from './add-collateral-modal/add-collateral-modal.component';
import { Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
@Component({
  selector: 'app-collateral',
  templateUrl: './collateral.component.html',
  styleUrls: ['./collateral.component.scss']
})
export class CollateralComponent implements OnInit {

  constructor(
    private router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  // Function for opening add collateral item
  openAddCollateralModal(): void {
    const dialogRef = this.dialog.open(AddCollateralModalComponent, {
      height: 'auto',
      width: '770px',
      data: { isEdit: false, loanStatusModel: null }
    });
    const sub = dialogRef.componentInstance.onNewCollateralDataRecieved.subscribe(
      (data: any) => {
        alert("ent emitted")
      });
  }
}
