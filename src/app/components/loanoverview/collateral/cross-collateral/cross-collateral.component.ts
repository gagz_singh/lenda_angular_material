import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';
import { serializearray } from '../../../../provider/commonmethods/appserializer';
import { environment } from '../../../../../environments/environment';
import { UB_CrossCollateralsModel } from '../../../../models/resmodel/cross-collaterals';
import { Loan } from '../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-cross-collateral',
  templateUrl: './cross-collateral.component.html',
  styleUrls: ['./cross-collateral.component.scss']
})
export class CrossCollateralComponent implements OnInit {
  localLoanObj: any;
  rows:Array<UB_CrossCollateralsModel>=new Array<UB_CrossCollateralsModel>();

  constructor(private localSt: LocalStorageService) { 
    this.localSt.observe(environment.loankey).subscribe(res=>{
       this.localLoanObj=<Loan>res;
       this.rows=this.localLoanObj.CrossCollaterals;
   })
  }

  ngOnInit() {
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    // this.localLoanObj = this.localSt.retrieve(environment.loankey);
     //.getCrossCollaterals();
  }

  getCrossCollaterals() {
    this.rows = this.localLoanObj.CrossCollaterals;

  }

}
