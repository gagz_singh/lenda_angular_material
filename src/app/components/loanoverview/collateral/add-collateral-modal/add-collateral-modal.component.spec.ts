import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCollateralModalComponent } from './add-collateral-modal.component';

describe('AddCollateralModalComponent', () => {
  let component: AddCollateralModalComponent;
  let fixture: ComponentFixture<AddCollateralModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCollateralModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCollateralModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
