import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { CollateralReqModel } from '../../../../models/reqmodel/collateral.model';
import { ToastsManager } from 'ng2-toastr';
import { LoanApiService } from '../../../../webservice/loanapi/loanapi.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../../environments/environment';
import { LoancalculationService } from '../../../../provider/calculations/loancalculation.service';
import { Loan } from '../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-add-collateral-modal',
  templateUrl: './add-collateral-modal.component.html',
  styleUrls: ['./add-collateral-modal.component.scss']
})
export class AddCollateralModalComponent implements OnInit {
  collateralModel: CollateralReqModel = new CollateralReqModel();
  loanId: number;
  isEdit:boolean;
  onNewCollateralDataRecieved = new EventEmitter();
  private sub: any;
  localLoanObj: any;
  constructor(

    private router: Router,
    private route: ActivatedRoute,
    private toaster: ToastsManager,
    private loanApiService: LoanApiService,
    public dialogRef: MatDialogRef<AddCollateralModalComponent>,
    public localSt: LocalStorageService,
    private loancalulationservice: LoancalculationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    debugger
    this.loanId = (<Loan>this.localSt.retrieve('currentselectedloan')).Id
  }

  ngOnInit() {
  }

  closeModal() {
    this.dialogRef.close();
  }

  // Function for adding new collateral item
  saveCollateral(form) {
    debugger
    if (!form.invalid) {
      if(this.loanId!=null && this.loanId!=undefined){

        this.collateralModel.LoanId = this.loanId;
      }
      else{
        this.toaster.error('Loan Object error');
        return
      }

      this.loanApiService.addCollateral(this.collateralModel).subscribe(res => {
        if (res.ResCode == 1) {

          this.collateralModel = new CollateralReqModel();
          this.closeModal();
          this.onNewCollateralDataRecieved.emit();
          this.toaster.success('Collateral saved successfully.');
        }
      })
    }
  }
}
