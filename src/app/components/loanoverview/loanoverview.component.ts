import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JsonConvert } from 'json2typescript';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '../../webservice/loanapi/loanapi.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { LoancalculationService } from '../../provider/calculations/loancalculation.service';

@Component({
  selector: 'app-loanoverview',
  templateUrl: './loanoverview.component.html',
  styleUrls: ['./loanoverview.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class LoanoverviewComponent implements OnInit {
  localLoanObj:any;
  jsonConvert: JsonConvert = new JsonConvert();
  loanid=0;
  constructor(private localSt:LocalStorageService,private loanservice:LoanApiService,private router:Router,private mainloanservice:LoancalculationService) {
    this.loanid=Number(router.url.substr(router.url.lastIndexOf('/') + 1));
   }

  ngOnInit() {
    //this.getLoanObject();
  }
  getLoanObject(){
    this.loanservice.getloansbyid(this.loanid).subscribe(res=>{
        if(res.ResCode==1){
          debugger
          // this.localSt.store(environment.loankey,res.Data);
          debugger
          this.mainloanservice.performcalculationonloanobject(res.Data);
        }
    });
    
  }


}
