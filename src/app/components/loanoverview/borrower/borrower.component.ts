import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UB_BalanceSheetItemModel, UB_IncomeRecordModel } from '../../../models/resmodel/balancesheetmodel';
import { JsonConvert } from 'json2typescript';
import { LoanApiService } from '../../../webservice/loanapi/loanapi.service';
import { BalanceSheetCalculationService } from './../../../provider/calculations/balancesheetcalculation.service';
import { environment } from '../../../../environments/environment';
import {LocalStorageService, LocalStorage} from 'ngx-webstorage';
import { serializeobject, serializearray } from '../../../provider/commonmethods/appserializer';
@Component({
  selector: 'app-borrower',
  templateUrl: './borrower.component.html',
  styleUrls: ['./borrower.component.scss']
})
export class borrowerComponent implements OnInit {
  localLoanObj:any;
  jsonConvert: JsonConvert = new JsonConvert();
  editingIR = {};
  loanid=0;
  rowsIR:Array<UB_IncomeRecordModel>=new Array<UB_IncomeRecordModel>();
  constructor(private localSt:LocalStorageService,private loanservice:LoanApiService,private route:ActivatedRoute) {
    this.loanid=this.route.snapshot.params.id;
   }

  ngOnInit() {
   
  }
}
