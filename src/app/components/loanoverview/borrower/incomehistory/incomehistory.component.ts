import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UB_IncomeRecordModel } from '../../../../models/resmodel/balancesheetmodel';
import { environment } from '../../../../../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancalculationService } from '../../../../provider/calculations/loancalculation.service';
import { Loan } from '../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-incomehistory',
  templateUrl: './incomehistory.component.html',
  styleUrls: ['./incomehistory.component.scss'],
  encapsulation :ViewEncapsulation.None
})
export class IncomehistoryComponent implements OnInit {
  localLoanObj:Loan;
  editingIR = {};
  rowsIR:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService,private loancalulationservice:LoancalculationService) { 
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rowsIR=this.localLoanObj.IncomeRecords;
   })
  }

  ngOnInit() {
    debugger
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.getIncomeRecords();
  }

  //region incomeHistory
    getIncomeRecords(){
    this.rowsIR=this.localLoanObj.IncomeRecords;
    }

    updateIRValue(event, cell, rowIndex) {
      this.editingIR[rowIndex + '-' + cell] = false;
      this.rowsIR[rowIndex][cell] = event.target.value;
      this.rowsIR = [...this.rowsIR];
      this.localLoanObj.IncomeRecords=this.rowsIR;
      this.loancalulationservice.performcalculationonloanobject(this.localLoanObj);
    }
    
   
}
