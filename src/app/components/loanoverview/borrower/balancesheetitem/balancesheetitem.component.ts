import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { UB_BalanceSheetItemModel } from '../../../../models/resmodel/balancesheetmodel';
import { environment } from '../../../../../environments/environment';
import { LoancalculationService } from './../../../../provider/calculations/loancalculation.service';
import { Loan } from './../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-balancesheetitem',
  templateUrl: './balancesheetitem.component.html',
  styleUrls: ['./balancesheetitem.component.scss'],
  encapsulation: ViewEncapsulation.None 
})
export class BalancesheetitemComponent implements OnInit {
  localLoanObj:Loan;
  editingBS = {};
  rowsBL:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService,private loancalulationservice:LoancalculationService) {
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rowsBL=this.localLoanObj.BalanceSheetItems;
   }
  )};
  ngOnInit() {
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.getbalancesheetitems();
  }
  getbalancesheetitems(){
    this.rowsBL=this.localLoanObj.BalanceSheetItems;
   }
   updateBSValue(event, cell, rowIndex) {
     this.editingBS[rowIndex + '-' + cell] = false;
     this.rowsBL[rowIndex][cell] =  isNaN(event.target.value)?event.target.value:Number(event.target.value);
     this.rowsBL = [...this.rowsBL];
     this.localLoanObj.BalanceSheetItems=this.rowsBL;
     this.loancalulationservice.performcalculationonloanobject(this.localLoanObj);
   }
}
