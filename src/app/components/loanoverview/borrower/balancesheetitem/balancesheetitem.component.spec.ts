import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalancesheetitemComponent } from './balancesheetitem.component';

describe('BalancesheetitemComponent', () => {
  let component: BalancesheetitemComponent;
  let fixture: ComponentFixture<BalancesheetitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalancesheetitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalancesheetitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
