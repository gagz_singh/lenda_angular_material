import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { JsonConvert } from 'json2typescript';
import { UB_CropProductionHistoryModel } from '../../../../models/resmodel/cropproductionhistorymodel';
import { serializearray } from '../../../../provider/commonmethods/appserializer';
import { environment } from '../../../../../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancropcalculationService } from '../../../../provider/calculations/loancropcalculation.service';
import { LoancalculationService } from './../../../../provider/calculations/loancalculation.service';
import { Loan } from '../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-croproductionhistory',
  templateUrl: './croproductionhistory.component.html',
  styleUrls: ['./croproductionhistory.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class CroproductionhistoryComponent implements OnInit {
  localLoanObj:Loan;
  editing = {};
  rows:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService,private loancalulationservice:LoancalculationService) { 
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rows=this.localLoanObj.LoanCrops;
   })
  }

  ngOnInit() {
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.getLoanCropitems();

  }

  getLoanCropitems(){
    this.rows=this.localLoanObj.LoanCrops;
   }
   updateValue(event, cell, rowIndex) {
     this.editing[rowIndex + '-' + cell] = false;
     this.rows[rowIndex][cell] =  isNaN(event.target.value)?event.target.value:Number(event.target.value);
     this.rows = [...this.rows];
     this.localLoanObj.LoanCrops=this.rows;
     this.loancalulationservice.performcalculationonloanobject(this.localLoanObj);
   }
  
}
