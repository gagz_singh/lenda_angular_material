import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CroproductionhistoryComponent } from './croproductionhistory.component';

describe('CroproductionhistoryComponent', () => {
  let component: CroproductionhistoryComponent;
  let fixture: ComponentFixture<CroproductionhistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CroproductionhistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CroproductionhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
