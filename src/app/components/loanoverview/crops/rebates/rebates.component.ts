import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../../environments/environment';
import { Loan } from '../../../../models/appmodels/loan.model';


@Component({
  selector: 'app-rebates',
  templateUrl: './rebates.component.html',
  styleUrls: ['./rebates.component.scss']
})
export class RebatesComponent implements OnInit {
  croptypes:Array<any>=new Array<any>();
  localLoanObj:any;
  rows:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService) { 
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rows=this.localLoanObj.Rebates;
   })
  }

  ngOnInit() {
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.getrebateitems();
  }
  getrebateitems(){
    this.rows=this.localLoanObj.Rebates;
   }
 
}
