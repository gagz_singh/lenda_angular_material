import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndirectCropIncomeComponent } from './indirect-crop-income.component';

describe('IndirectCropIncomeComponent', () => {
  let component: IndirectCropIncomeComponent;
  let fixture: ComponentFixture<IndirectCropIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndirectCropIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndirectCropIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
