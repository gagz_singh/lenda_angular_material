import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';
import { serializearray } from '../../../../provider/commonmethods/appserializer';
import { environment } from '../../../../../environments/environment';
import { UB_IndirectCropIncomeModel } from '../../../../models/resmodel/indirect-crop-income';
import { Loan } from '../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-indirect-crop-income',
  templateUrl: './indirect-crop-income.component.html',
  styleUrls: ['./indirect-crop-income.component.scss']
})
export class IndirectCropIncomeComponent implements OnInit {
  localLoanObj: any;
  rows:Array<UB_IndirectCropIncomeModel>=new Array<UB_IndirectCropIncomeModel>();
  constructor(private localSt: LocalStorageService) { 
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rows=this.localLoanObj.IndirectIncomes;
   })
  }

  ngOnInit() {
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    // this.localLoanObj = this.localSt.retrieve(environment.loankey);
     this.getIndirectCropIncome();
  }
  getIndirectCropIncome() {
    debugger
    this.rows = this.localLoanObj.IndirectIncomes;

  }
}
