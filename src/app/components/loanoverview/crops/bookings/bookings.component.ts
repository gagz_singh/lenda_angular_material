import { Component, OnInit } from '@angular/core';
import { JsonConvert } from 'json2typescript';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../../environments/environment';
import { UB_BookingModel } from '../../../../models/resmodel/cropproductionhistorymodel';
import { Loan } from './../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {
  croptypes:Array<any>=new Array<any>();
  localLoanObj:Loan;
  rows:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService) { }

  ngOnInit() {
    debugger
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.croptypes=this.localSt.retrieve("croptypes");
    this.getbuyerrebateitems();
  }
  getbuyerrebateitems(){
    debugger
    this.rows=this.localLoanObj.Bookings;
   }
   getcropname(id:number){
    return this.croptypes.find(p=>p.id==this.rows[id].CropId).value;
  }

  getbuyername(id:number){
    debugger
    return this.localLoanObj.ContractEntities.find(p=>p.Id==this.rows[id].BuyerId).Name;
  }
}
