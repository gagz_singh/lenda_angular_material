import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RebateentitiesComponent } from './rebateentities.component';

describe('RebateentitiesComponent', () => {
  let component: RebateentitiesComponent;
  let fixture: ComponentFixture<RebateentitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RebateentitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RebateentitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
