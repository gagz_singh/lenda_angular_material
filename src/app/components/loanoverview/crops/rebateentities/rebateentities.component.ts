import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { JsonConvert } from 'json2typescript';
import { LocalStorageService } from 'ngx-webstorage';
import { UB_BuyerRebateModel } from '../../../../models/resmodel/cropproductionhistorymodel';
import { Loan } from './../../../../models/appmodels/loan.model';

@Component({
  selector: 'app-rebateentities',
  templateUrl: './rebateentities.component.html',
  styleUrls: ['./rebateentities.component.scss']
})
export class RebateentitiesComponent implements OnInit {
  localLoanObj:Loan;
  rows:Array<any>=new Array<any>();
  constructor(private localSt:LocalStorageService) {
    this.localSt.observe(environment.loankey).subscribe(res=>{
      debugger
       this.localLoanObj=<Loan>res;
       this.rows=this.localLoanObj.ContractEntities;
   })
   }

  ngOnInit() {
    debugger
    this.localLoanObj=<Loan>(this.localSt.retrieve(environment.loankey));
    this.getbuyerrebateitems();
  }
  getbuyerrebateitems(){
    debugger
    this.rows=this.localLoanObj.ContractEntities;
   }
   
}
