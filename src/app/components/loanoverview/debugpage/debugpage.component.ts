import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Loan } from './../../../models/appmodels/loan.model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../environments/environment';
import { InsuranceCalculationService } from '../../../provider/calculations/insurancecalculation.service';
import { LoanApiService } from '../../../webservice/loanapi/loanapi.service';
import { LoancalculationService } from '../../../provider/calculations/loancalculation.service';

@Component({
  selector: 'app-debugpage',
  templateUrl: './debugpage.component.html',
  styleUrls: ['./debugpage.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class DebugpageComponent implements OnInit {
  loanid:number=0;
  private gridApi;
  private gridColumnApi;
  public MPCITotal=0;
  private localLoanObj:Loan;
  public columnDefs;
  rowData:any;
  public frameworkComponents;
  constructor(public localst:LocalStorageService,public insurancecalculation:InsuranceCalculationService,private loanservice:LoanApiService,private mainloanservice:LoancalculationService) {
    this.localst.observe(environment.loankey).subscribe(res=>{
      this.localLoanObj=<Loan>res;
      debugger
      this.rowData=this.localLoanObj.FC_MPCIValues;
      debugger
      if(this.rowData != undefined)
      this.getsumofMPCI();
  })
}

  ngOnInit() {
    this.frameworkComponents = {

    };
    this.localLoanObj=<Loan>(this.localst.retrieve(environment.loankey));
    this.columnDefs = [
      {
        headerName: "MPCI Values",
        children: [
          {
            headerName: "CropName",
            field: "CropName",
            width:150
          },
          {
            headerName: "LoanId",
            field: "LoanId",
           width:180
          },
          {
            headerName: "Acres",
            field: "Acres",
           width:180
          },
          {
            headerName: "FSNId",
            field: "FSNId",
           width:180
          },
          {
            headerName: "MPCI",
            field: "MPCI",
           width:180
          },{
            headerName:"APH Used",
            field :"APH",
            width:100
          },
          {
            headerName:"Practice",
            field :"Practice",
            width:100
          },
          {
            headerName:"Price",
            field:"Price",
            width:100
          
          },
          {
            headerName:"Premium",
            field:"Premium",
            width:100
          
          },
          {
            headerName:"Policylevel",
            field:"Policylevel",
            width:100
          
          }
        ]
      }
    ];

  }

  onGridReady(params) {
    this.loanid=this.localLoanObj.Id;
    // this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // params.api.setRowData(this.localLoanObj.FC_MPCIValues);
    debugger
    this.rowData=this.localLoanObj.FC_MPCIValues;
    if(this.rowData != undefined)
     this.getsumofMPCI();
  }

  getLoanObject(){
    this.loanservice.getloansbyid(this.loanid).subscribe(res=>{
        if(res.ResCode==1){
          debugger
           this.localst.store(environment.loankey,res.Data);
          debugger
          this.mainloanservice.performcalculationonloanobject(res.Data);
        }
    });
    
  }
  calculateMPCI(){
    this.getLoanObject();
    
  }

  getsumofMPCI(){
    let value= this.localLoanObj.FC_MPCIValues.reduce((p,n)=>{
         return p+(n.MPCI==undefined?0:n.MPCI);
    },0)
    debugger
    this.MPCITotal=Math.round(value);
  }
}
