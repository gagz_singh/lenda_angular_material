import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebugpageComponent } from './debugpage.component';

describe('DebugpageComponent', () => {
  let component: DebugpageComponent;
  let fixture: ComponentFixture<DebugpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebugpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebugpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
