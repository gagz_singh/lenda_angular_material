import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanobjectpreviewComponent } from './loanobjectpreview.component';

describe('LoanobjectpreviewComponent', () => {
  let component: LoanobjectpreviewComponent;
  let fixture: ComponentFixture<LoanobjectpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanobjectpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanobjectpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
