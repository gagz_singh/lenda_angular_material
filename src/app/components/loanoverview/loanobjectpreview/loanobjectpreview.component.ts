import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from './../../../../environments/environment.prod';

@Component({
  selector: 'app-loanobjectpreview',
  templateUrl: './loanobjectpreview.component.html',
  styleUrls: ['./loanobjectpreview.component.scss']
})
export class LoanobjectpreviewComponent implements OnInit {
  currentloanobject:any;
  constructor(private localst:LocalStorageService) { }

  ngOnInit() {
  }

  loancurrentloanobject(){
  this.currentloanobject=this.localst.retrieve(environment.loankey);
  }
}
