import { Component, OnInit } from '@angular/core';
import { LoanApiService } from './../../../webservice/loanapi/loanapi.service';
import { UB_LoanListShort } from '../../../models/resmodel/loanmodels';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.scss']
})
export class LoanListComponent implements OnInit {
   private previndex = 0;
   private pagesize = 10;
   public loanlist:Array<any>=new Array<any>();
   public pageheader='Loan Listing';
  constructor(private loanService: LoanApiService,private route:Router) { }

  ngOnInit() {
    debugger
    this.getloanlist();
  }

  getloanlist()
  {
  this.loanService.getloanslistshort(this.pagesize, this.previndex).subscribe(res=>{
    if(res.ResCode==1){
      this.loanlist=res.Data;
      debugger
    }
  });
  }

  navigatetoloan(id:number){
    this.route.navigate(['/loanoverview/borrower',id]);
  }

}
