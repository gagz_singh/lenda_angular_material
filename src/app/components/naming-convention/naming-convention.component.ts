import { Component, OnInit } from '@angular/core';

import { Loan } from '../../models/appmodels/loan.model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment';
import { LoancalculationService } from '../../provider/calculations/loancalculation.service';
import { HttpClient } from '@angular/common/http';
import { ChildrenOutletContexts } from '@angular/router';
import { NumericEditor } from '../../aggridfilters/numericaggrid';
import { Validators } from '@angular/forms';
import { MasterApiService } from '../../services/masterapi.service';
import { NamingConventionEntity } from '../../models/appmodels/customodels';
import { AggridTxtAreaComponent } from '../../aggridfilters/textarea';

@Component({
  selector: 'app-naming-convention',
  templateUrl: './naming-convention.component.html',
  styleUrls: ['./naming-convention.component.scss']
})

export class NamingConventionComponent implements OnInit {
  public loading = false;
  private gridApi;
  public defaultColDef;
  public btndsb: boolean = true;
  private gridColumnApi;
  public columnDefs;
  public selectedrows = [];
  public frameworkComponents;
  localLoanObj: Loan;
  public rowSelection;
  editing = {};
  rows = [];

  

  constructor(
    private localSt: LocalStorageService,
    private loancalulationservice: LoancalculationService,
    private masterApiService: MasterApiService
  ) {

    this.localSt.observe(environment.loankey).subscribe(res => {
      this.localLoanObj = <Loan>res;
    })
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.rows);
    setTimeout(function() {
      params.api.resetRowHeights();
    }, 500);
  }

  ngOnInit() {
    // this.frameworkComponents = {
    //   numericEditor: NumericEditor
    // };
    this.localLoanObj = <Loan>(this.localSt.retrieve(environment.loankey));
    this.columnDefs = [

      {
        headerName: "Id",
        field: "Id",
        width: 80,
        checkboxSelection: true,
        editable: true
      },
      {
        headerName: "Old_LM_Table",
        field: "Old_LM_Table",
        width: 150,
        editable: true
      },
      {
        headerName: "Old_LM_Field",
        field: "Old_LM_Field",
        width: 150,
        editable: true
      },
      {
        headerName: "New_LM_Table",
        field: "New_LM_Table",
        width: 150,
        editable: true
      },
      {
        headerName: "New_LM_Field",
        field: "New_LM_Field",
        width: 150,
        editable: true
      },
      {
        headerName: "Seq_Num",
        field: "Seq_Num",
        width: 100,
        editable: true
      },
      {
        headerName: "Formula",
        field: "Formula",
        width: 200,
        editable: true
      },
      {
        headerName: "LoanObject",
        field: "LoanObject",
        width: 150, editable:  true,
        cellStyle: { "white-space": "normal" },
      },
      {
        headerName: "Level",
        field: "Level",
        width: 150, editable:  true,
        cellStyle: { "white-space": "normal" },
      },
      {
        headerName: "LendaPlusName",
        field: "LendaPlusName",
        width: 150, editable:  true,
        cellStyle: { "white-space": "normal" },
      },
      {
        headerName: "Comments",
        field: "Comments",
        cellStyle: { "white-space": "normal" },
        autoHeight:true,
        cellEditor:'agLargeTextCellEditor',
        width: 250, editable:  true
      }
    ];
    
    this.rowSelection = "multiple";
    this.getNamingConventionList();
  }

  getNamingConventionList() {
    
    this.masterApiService.getNamingConventionList().subscribe(res => {
      if (res.ResCode == 1) {
        this.rows = res.Data;
        if(res.Data==null){
          this.rows=[];
        }
        this.rows.push({});
      }
    })
  }
  onSelectionChanged() {
    debugger
    this.selectedrows = this.gridApi.getSelectedRows();
    if (this.selectedrows.length > 0) {
      this.btndsb = false;
    }
    else {
      this.btndsb = true;
    }
  }


celleditingstopped(event: any) {
  debugger
if(event.value.trim()!=""){
  this.loading=true;
  if (event.data.Id == undefined) {
    
    var newItem ={};
    event.data.Id = 0;
    this.masterApiService.addEditnamingConvention(event.data).subscribe(res => {
      this.rows[event.rowIndex]=event.data;
      this.rows[event.rowIndex].Id = parseInt(res.Data);
      this.gridApi.setRowData(this.rows);
      this.gridApi.updateRowData({ add: [newItem] });
      this.loading=false;
    });
  }
  else {
    this.masterApiService.addEditnamingConvention(event.data).subscribe(res => {
      this.loading=false;
    });
  }
 
}
}

search(event:any){
    
  this.gridApi.setQuickFilter(event.target.value);
}
Deleterow(){
  this.selectedrows.forEach(element => {
    this.masterApiService.deletenamingConvention(element.Id).subscribe(res => {
      this.rows = this.rows.filter(p => p.Id != element.Id);
    })
  });
}
}